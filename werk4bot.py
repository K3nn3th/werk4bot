#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging, requests

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import  ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram import InlineKeyboardMarkup, InlineKeyboardButton as Button

import telegramcalendar

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

class TableEntry(object):
    def __init__(self, name='', location='', date='', time='', duration='', groupsize=''):
        super(TableEntry, self).__init__()
        self.name = name
        self.location = location
        self.date = date
        self.time = time
        self.duration = duration
        self.groupsize = groupsize

    def is_ready(self):
        result = False
        if self.name != '' and self.location != '' and self.date !='' and self.time != '' and self.duration != '' and self.groupsize != '':
            result = True
        return result

url_base = 'https://www.directjuice.org'
url_add = '/set_entry.php'
url_del = '/remove_entry.php'
url_spec = dict()
users = {}

def return_entries_json(date):
    json_entries = [{"date":"2020-06-19","name":"Robert","groupsize":"2","starttime":"10:00:00","endtime":"12:00:00"},
                    {"date":"2020-06-19","name":"argf","groupsize":"2","starttime":"12:00:00","endtime":"13:00:00"},
                    {"date":"2020-06-21","name":"LLLK","groupsize":"2","starttime":"10:00:00","endtime":"12:00:00"},
                    {"date":"2020-06-22","name":"Matze","groupsize":"2","starttime":"16:00:00","endtime":"17:00:00"},
                    {"date":"2020-06-22","name":"Thomas","groupsize":"2","starttime":"17:00:00","endtime":"19:00:00"}]
    results = []
    for entry in json_entries:
        #print('comparing: ' + date + ' and ' + entry['date'])
        if entry['date'] == date:
            print(' found')
            results.append(entry)

    return results

def inline_handler(update, context):
    print('---' + update.callback_query.data + '\n---')
    #selected, date = telegramcalendar.process_calendar_selection(context.bot, update)
    query = update.callback_query
    data = query.data.split(';')
    user = users[query.message.chat_id]

    if data[0] == 'ACC':
        context.bot.edit_message_text(text=query.message.text,
                chat_id=query.message.chat_id,
                message_id=query.message.message_id
            )
        if data[1] == 'SEND':
            name = update.callback_query.from_user.first_name
            url = 'http://directjuice.org/set_entry.php?date=2020-06-21&time=19&duration=1&name=' + name + '&groupsize=1'
            requests.get(url)

        elif data[1] == 'CANCEL':
            c_id = update.callback_query.message.chat_id
            user = users[c_id]
            m_id = user['info_id']
            # delete info_id message
            context.bot.delete_message(chat_id=update.callback_query.from_user.id, message_id = m_id)
#                    text = "Cancelled. No Session Noted.",
#                    reply_markup = ReplyKeyboardRemove())

    elif data[0] == 'LOC':
        context.bot.edit_message_text(text=query.message.text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id
            )
        user['entry'].location = data[1]
        update_info_message(update, context, telegramcalendar.create_week_calendar())
        #context.bot.send_message(chat_id=update.callback_query.from_user.id,
        #            text = "Location: %s" % (data[1]),
        #            reply_markup = telegramcalendar.create_week_calendar())
        context.bot.answer_callback_query(query.id, text='%s chosen.' % (data[1]), show_alert=False)
        print(user['entry'].location)
        #url_spec['location'] = data[1]
        return

    elif data[0] == 'TIME':
        if data[1] == 'TAKEN':
            # print 'taken! choose another time'
            context.bot.answer_callback_query(query.id, text='This slot is taken by %s.\nChoose another.' % (data[2]), show_alert=True)
            pass
        else:
            context.bot.edit_message_text(text=query.message.text,
                chat_id=query.message.chat_id,
                message_id=query.message.message_id
                )
            user['entry'].time = data[1] + " o'clock"
            update_info_message(update, context, duration_menu_keyboard(data[2]))
            #context.bot.send_message(chat_id=update.callback_query.from_user.id,
            #                text="Time: %s o'clock" % (data[1]),
            #                reply_markup=duration_menu_keyboard(data[2]))
            #url_spec['time'] = data[1]
            context.bot.answer_callback_query(query.id, text='%s chosen.' % (data[1]), show_alert=False)
            return

    elif data[0] == 'DUR':
        context.bot.edit_message_text(text=query.message.text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id
            )
        user['entry'].duration = data[1]
        update_info_message(update, context, group_menu_keyboard())
        #context.bot.send_message(chat_id=update.callback_query.from_user.id,
        #                text="Duration: %s" % (data[1]),
        #                reply_markup=group_menu_keyboard())
        context.bot.answer_callback_query(query.id, text='%s chosen.' % (data[1]), show_alert=False)
        #url_spec['duration'] = data[1]
        return

    elif data[0] == 'GRP':
        context.bot.edit_message_text(text=query.message.text,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id
            )
        print('sending: \n' + str(url_spec))
        user['entry'].groupsize = data[1]
        update_info_message(update, context, accept_menu_keyboard())

        #context.bot.send_message(chat_id=update.callback_query.from_user.id,
        #                text="Group Size: " + str(data[1]) + """\nAre the details OK?
#
#                        """,
#                        reply_markup=accept_menu_keyboard())
        context.bot.answer_callback_query(query.id, text='%s chosen.' % (data[1]), show_alert=False)
        #url_spec['group_size'] = data[1]
        return

    else:
        # for calendar specific stuff
        selected, date = telegramcalendar.process_calendar_selection(context.bot, update)
        nice_date = date.strftime("%Y-%m-%d")
        if selected:
            user['entry'].date = data[3] + "." + data[2] + '.' + data[1]
            update_info_message(update, context, time_menu_keyboard(nice_date))

            context.bot.answer_callback_query(query.id, text='%s chosen.' % (data[1]), show_alert=False)
            #url_spec['date'] = nice_date

def update_info_message(update, context, markup):
   # init_user(c_id, update.message.from_user.first_name)
    c_id = update.callback_query.message.chat_id
    user = users[c_id]
    m_id = user['info_id'] # message id to edit
    #print('updating: ' + str(m_id))
    entry = user['entry']
    location = entry.location
    print('from update: ' + str(location) + '__')
    info_string = 'New Session\n\nLocation: '
    if entry.location != '':
        info_string += (entry.location + '\nDate: ')
    if entry.date != '':
        info_string += (entry.date + '\nTime: ')
    if entry.time != '':
        info_string += (entry.time + '\nDuration: ')
    if entry.duration != '':
        info_string += (entry.duration + '\nGroup Size: ')
    if entry.groupsize != '':
        info_string += (entry.groupsize)
    #info_string = 'changed.'
    #user['entry']
    context.bot.edit_message_text(text=info_string,
        chat_id=c_id,
        message_id=m_id,
        reply_markup=markup
    ) # vales to use

def add_entry(update, context):
    url_spec = []
    """Send a message when the command /add is issued."""

    init_user(update.message.chat_id, update.message.from_user.first_name)
    c_id = update.message.chat_id
    users[c_id]['entry'] = TableEntry()
    result = update.message.reply_text('New Session\n\nLocation:',
                    reply_markup=location_menu_keyboard())
    m_id = result['message_id']
    users[update.message.chat_id]['info_id'] = m_id
    print('addinf: ' + str(m_id))


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    name = update.message.from_user.first_name
    init_user(update.message.chat_id, name)
    update.message.reply_text('Hi, \Here is the command list to tell me what to do.\n/add to schedule a skate session (Your user name "' + name + '" will be used)\n/name to change the name used for scheduling (example: /name Geoff R.)')

def init_user(chat_id, name): #, tongue):
	try:
		users[chat_id]
	except KeyError:
		users[chat_id] = {
			"name": name
		}

def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text("""Help! How do I use this?
     /add tells the werk4bot when you would like to skate in the hall.
     /test_add will test writing to the DB.
     /test_remove will test adding and removing an entry to/from the DB.
    """)

def accept_menu_keyboard():
    buttons = [
        [
            Button(str(i), callback_data='ACC;' + str(i)) for i in ['SEND', 'CANCEL']
        ],
    ]
    return InlineKeyboardMarkup(buttons)

def duration_menu_keyboard(max_time):
    buttons = [
        [
            Button(str(i + 1) + 'h', callback_data='DUR;' + str(i + 1)) for i in range(int(max_time))
        ],
    ]
    return InlineKeyboardMarkup(buttons)

def group_menu_keyboard():
    buttons = [
        [
            Button(str(i + 1), callback_data='GRP;' + str(i + 1)) for i in range(10)
        ],
    ]
    return InlineKeyboardMarkup(buttons)

def location_menu_keyboard():
    buttons = [
        [
            Button(str(i), callback_data='LOC;' + str(i)) for i in ['Indoors', 'Outdoors']
        ],
    ]
    return InlineKeyboardMarkup(buttons)

# this is for disabling times which are taken already
def create_timeslot_callback(time, entries):
    present_names = is_free_timeslot(time, entries)
    present_names_next = is_free_timeslot(time + 1, entries)
    if present_names == []:
        free = '1'
        if present_names_next == []:
            free = '2'
        return 'TIME;' + str(time) + ":00;" + free
    else:
        return 'TIME;TAKEN;' + present_names

def create_timeslot_label(time, entries):
    present_names = is_free_timeslot(time, entries)
    if  present_names == []:
        return str(time) + ':00'
    else:
        return '--'


def is_free_timeslot(time, json_entries):
    for e in json_entries:
        start = int(e['starttime'].split(':')[0])
        end = int(e['endtime'].split(':')[0])
        #print('comparing ' + str(start) + ' <= ' + str(time) + ' < ' + str(end))
        if  start <= int(time) and end > int(time):
            return e['name']
    return  []

def time_menu_keyboard(date):
    vacants = return_entries_json(date)
    buttons = [
        [
            Button(create_timeslot_label(i, vacants), callback_data = create_timeslot_callback(i, vacants)) for i in range(6)
        ],
        [
            Button(create_timeslot_label(i, vacants), callback_data=create_timeslot_callback(i, vacants)) for i in range(6, 12)
        ],
        [
            Button(create_timeslot_label(i, vacants), callback_data=create_timeslot_callback(i, vacants)) for i in range(12, 18)
        ],
        [
            Button(create_timeslot_label(i, vacants), callback_data=create_timeslot_callback(i, vacants)) for i in range(18, 23)
        ],
    ]
    return InlineKeyboardMarkup(buttons)
    #return ReplyKeyboardMarkup(buttons, one_time_keyboard=True)


def echo(update, context):
    """Echo the user message."""
    update.message.reply_text(update.message.text)

def name(update, context):
    """Set the user name."""
    new_name = update.message.text.split('/name')[1].strip()
    chat_id = update.message.chat_id
    users[chat_id]['name'] = new_name
    update.message.reply_text('Name set to: ' + users[chat_id]['name'])

def test_add(update, context):
    chat_id = update.message.chat_id
    name = users[chat_id]['name']
    url = 'http://directjuice.org/set_entry.php?date=2020-06-21&time=19&duration=1&name=' + name + '&groupsize=1'
    update.message.reply_text('sending request to url: ' + url)
    requests.get(url)

def test_remove(update, context):
    test_add(update, context)
    url = 'http://directjuice.org/remove_entry.php?date=2020-06-21&time=19'
    update.message.reply_text('sending request to url: ' + url)
    requests.get(url)

def main():

    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("1259457009:AAF1FBXaFsKEWBclG5N3t6wjR59lCPxIQBE", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("test_add", test_add))
    dp.add_handler(CommandHandler("test_remove", test_remove))
    dp.add_handler(CommandHandler("name", name))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("add", add_entry))
    dp.add_handler(CallbackQueryHandler(inline_handler))

    # on noncommand i.e message - echo the message on Telegram
    #dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()